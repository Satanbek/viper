import json
from datetime import datetime

import re

import telebot
from django.conf import settings
from django.db import transaction
from django.db.models import Count, Q, F
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt

from telebot.apihelper import ApiException
from telebot.types import ReplyKeyboardRemove

from .models import (
    BotUser, Category, Specialist, Vote,
    Promo, Review, Charges, GroomGuests,
    BrideGuests, Feedback,
    SpecialistApplication, BotConfiguration,
    City
)

MAIN_MENU = [
    'Личный кабинет', 'Специалисты',
    'Поиск', 'Обзоры',
    'События и акции', 'Органайзер',
    'Обратная связь', 'Заявка на размещение'
]


def show_personal_information(bot, user):
    keyboard = build_keyboard(
        buttons=[
            'Изменить имя и город',
            'Установить фото пары',
            'Указать дату свадьбы',
            'Избранное'
        ]
    )

    wedding_date = (user.wedding_date - datetime.now().date()).days if user.wedding_date else 'не указано'

    city = user.city if user.city else 'не указано'

    reply_message = f'Ваше имя: {user.get_name}\n' \
                    f'Ваш город: {city}\n' \
                    f'Дней до свадьбы: {wedding_date}\n\n' \
                    f'Вам помогает бот @Pastila_bot!'

    if not user.pair_photo_file_id:
        bot.send_message(
            chat_id=user.chat_id,
            text=reply_message,
            reply_markup=keyboard
        )
    else:
        bot.send_photo(
            chat_id=user.chat_id,
            photo=user.pair_photo_file_id,
            caption=reply_message,
            reply_markup=keyboard
        )

    user.step = 'personal_information'
    user.save()


@csrf_exempt
def webhook_view(request):
    if not request.method == 'POST':
        return HttpResponse(status=415)

    bot = telebot.TeleBot(settings.TELEGRAM_TOKEN)

    data = json.loads(request.body.decode('utf-8'))

    try:

        if 'callback_query' in data:
            chat_id = data["callback_query"]["from"]["id"]
            callback_data = data["callback_query"]["data"]
            from_user = data["callback_query"]["from"]
        elif 'message' in data:

            if 'text' in data['message'].keys():
                message = data['message']
                chat_id = message['chat']['id']
                from_user = message['from']
                callback_data = "empty"

    except KeyError:
        print('No from_user, text or message')
        return HttpResponse(status=200)
    else:
        if 'callback_query' in data:
            text = ''
        else:
            text = message.get('text', '')

    user = get_bot_user(from_user)
    config = BotConfiguration.get_solo()

    if text == '/start':
        start_message = config.start_message.strip() or 'Здравствуйте! Вас приветствует бот!'
        bot.send_message(
            chat_id=chat_id,
            text=start_message
        )

        ask_for_name(bot, user)

        return HttpResponse(status=200)

    if text == 'Меню':
        keyboard = get_main_menu()

        bot.send_message(
            chat_id=chat_id,
            text=text,
            reply_markup=keyboard
        )

        user.step = ''
        user.save()

        return HttpResponse(status=200)

#############################333
    ########################3
################################

    if callback_data.find('delete_charge') != -1:

        charge_id = int(callback_data[13:])

        Charges.objects.filter(id = charge_id).delete()

        mes = 'Данные завтраты удалены!'
        bot.edit_message_text(chat_id=data["callback_query"]["message"]["chat"]["id"], message_id=data["callback_query"]["message"]["message_id"], text=mes)

        return HttpResponse(status=200)

    if callback_data.find('edit_charge') != -1:

        charge_id = int(callback_data[11:])

        mes = 'Введите новые затраты:'
        bot.send_message(chat_id=user.chat_id, text=mes)

        user.step = 'edit_charge'+str(charge_id)
        user.save()

        return HttpResponse(status=200)

    if user.step.find('edit_charge') != -1:
        try:
            charge_id = int(user.step[11:])
            print(charge_id)
            charge = Charges.objects.filter(id = charge_id).first()

            charge.text = text
            charge.save()

            user.step = 'show_charges'
            user.save()

            mes = 'Данные затраты отредактированы.'
            bot.send_message(chat_id=user.chat_id, text=mes)

            return HttpResponse(status=200)
        except:
            return HttpResponse(status=200)


    if callback_data.find('delete_bride_guest') != -1:


        bride_guest_id = int(callback_data[18:])

        BrideGuests.objects.filter(id = bride_guest_id).delete()

        mes = 'Данные гости удалены!'
        bot.edit_message_text(chat_id=data["callback_query"]["message"]["chat"]["id"], message_id=data["callback_query"]["message"]["message_id"], text=mes)

        return HttpResponse(status=200)

    if callback_data.find('edit_bride_guest') != -1:

        bride_guest_id = int(callback_data[16:])

        mes = 'Введите новые затраты:'
        bot.send_message(chat_id=user.chat_id, text=mes)

        user.step = 'edit_bride_guest'+str(bride_guest_id)
        user.save()

        return HttpResponse(status=200)

    if user.step.find('edit_bride_guest') != -1:
        try:
            bride_guest_id = int(user.step[16:])
            bride_guest = BrideGuests.objects.filter(id = bride_guest_id).first()

            bride_guest.text = text
            bride_guest.save()

            user.step = 'show_bride_guests'
            user.save()

            mes = 'Данные гости отредактированы.'
            bot.send_message(chat_id=user.chat_id, text=mes)

            return HttpResponse(status=200)
        except Exception as e:
            print(str(e))
            return HttpResponse(status=200)


    if callback_data.find('delete_groom_guest') != -1:


        groom_guest_id = int(callback_data[18:])

        GroomGuests.objects.filter(id = groom_guest_id).delete()

        mes = 'Данные гости удалены!'
        bot.edit_message_text(chat_id=data["callback_query"]["message"]["chat"]["id"], message_id=data["callback_query"]["message"]["message_id"], text=mes)

        return HttpResponse(status=200)

    if callback_data.find('edit_groom_guest') != -1:

        groom_guest_id = int(callback_data[16:])

        mes = 'Введите новые затраты:'
        bot.send_message(chat_id=user.chat_id, text=mes)

        user.step = 'edit_groom_guest'+str(groom_guest_id)
        user.save()

        return HttpResponse(status=200)

    if user.step.find('edit_groom_guest') != -1:
        try:
            groom_guest_id = int(user.step[16:])
            groom_guest = GroomGuests.objects.filter(id = groom_guest_id).first()

            groom_guest.text = text
            groom_guest.save()

            user.step = 'show_groom_guests'
            user.save()

            mes = 'Данные гости отредактированы.'
            bot.send_message(chat_id=user.chat_id, text=mes)

            return HttpResponse(status=200)
        except:
            return HttpResponse(status=200)


    if user.step == 'input_name':
        name = text.strip()

        if not name:
            bot.send_message(
                chat_id=chat_id,
                text='Пожалуйста, укажите свое имя!'
            )
            return HttpResponse(status=200)

        user.name = name

        bot.send_message(
            chat_id=chat_id,
            text=f'Имя *{name}* сохранено.\n\n',
            parse_mode='Markdown'
        )

        ask_for_city(bot, user)

        return HttpResponse(status=200)

    if user.step == 'input_city':
        city = text.strip()

        if not city:
            bot.send_message(
                chat_id=chat_id,
                text='Пожалуйста, выберите свой город!'
            )
            return HttpResponse(status=200)

        if city == 'Указать позже':
            bot.send_message(
                chat_id=chat_id,
                text='Вы можете указать Ваш город в "Личном кабинете".'
            )
            user.city = None
            user.save()

            show_menu(bot, user)
            return HttpResponse(status=200)

        try:
            city_obj = City.objects.get(name=city)
        except City.DoesNotExist:
            bot.send_message(
                chat_id=chat_id,
                text='Пожалуйста, выберите свой город!'
            )
            return HttpResponse(status=200)
        else:
            user.city = city_obj
            user.save()
            show_menu(bot, user)

            bot.send_message(
                chat_id=chat_id,
                text=f'Город *{city}* сохранен.\n\n',
                parse_mode='Markdown'
            )

        return HttpResponse(status=200)

    if user.step == 'input_wedding_date':
        try:
            date = datetime.strptime(text.strip(), '%d.%m.%Y').date()

            if date < datetime.now().date():
                raise ValueError('Wedding date can not be set in the past')

        except ValueError:
            bot.send_message(
                chat_id=chat_id,
                text='Некорректная дата! Убедитесь, что указанная дата еще не наступила, '
                     'а формат совпадает с примером: *01.01.2019*',
                parse_mode='Markdown'
            )
        else:
            user.wedding_date = date

            bot.send_message(
                chat_id=chat_id,
                text=f'Дата свадьбы установлена на *{text}*.\n\n',
                parse_mode='Markdown'
            )

            show_personal_information(bot, user)

        return HttpResponse(status=200)

    if user.step == 'input_pair_photo':
        try:
            file_id = message['photo'][-1]['file_id']
        except KeyError:
            bot.send_message(
                chat_id=chat_id,
                text=f'Вам нужно отправить фотографию!\n'
                     f'Убедитесь, что во время загрузки Вы выбрали "Фото", а не "Файл"!\n\n'
                     f'Отправьте мне фотографию Вашей пары:',
            )
            return HttpResponse(status=200)
        else:
            user.pair_photo_file_id = file_id

            bot.send_message(
                chat_id=chat_id,
                text=f'фото Вашей пары сохранено!'
            )

            show_personal_information(bot, user)
            return HttpResponse(status=200)

    if text == 'Обратная связь':
        feedback_message = config.feedback_message or 'Введите Ваше сообщение или вопрос администратору'
        bot.send_message(
            chat_id=chat_id,
            text=feedback_message,
            reply_markup=ReplyKeyboardRemove()
        )

        user.step = 'input_feedback'
        user.save()

        return HttpResponse(status=200)

    if user.step == 'input_feedback':
        Feedback.objects.create(
            text=text,
            user=user
        )

        bot.send_message(
            chat_id=chat_id,
            text='Ваше сообщение отправлено',
            reply_markup=ReplyKeyboardRemove()
        )

        show_menu(bot, user)
        return HttpResponse(status=200)

    if text == 'Заявка на размещение':
        bot.send_message(
            chat_id=chat_id,
            text='Напишите Вашу специализацию, имя и телефон. Наш менеджер свяжется с Вами.',
            reply_markup=ReplyKeyboardRemove()
        )

        user.step = 'input_specialist_application'
        user.save()

        return HttpResponse(status=200)

    if user.step == 'input_specialist_application':
        SpecialistApplication.objects.create(
            text=text,
            user=user
        )

        bot.send_message(
            chat_id=chat_id,
            text='Ваша заявка на размещение отправлена администратору'
        )

        show_menu(bot, user)
        return HttpResponse(status=200)

    if text == 'Личный кабинет':
        show_personal_information(bot, user)
        return HttpResponse(status=200)

    if user.step == 'personal_information':
        if text == 'Изменить имя и город':
            ask_for_name(bot, user)
            return HttpResponse(status=200)

        if text == 'Указать дату свадьбы':
            bot.send_message(
                chat_id=chat_id,
                text='Введите дату Вашей свадьбы в формате *01.01.2019*:',
                parse_mode='Markdown',
                reply_markup=ReplyKeyboardRemove()
            )

            user.step = 'input_wedding_date'
            user.save()

            return HttpResponse(status=200)

        if text == 'Установить фото пары':
            bot.send_message(
                chat_id=chat_id,
                text='Отправьте фотографию Вашей пары:',
                reply_markup=ReplyKeyboardRemove()
            )

            user.step = 'input_pair_photo'
            user.save()

            return HttpResponse(status=200)

        if text == 'Избранное':
            show_favorite_specialists(bot, user)
            return HttpResponse(status=200)

        if text == 'Назад':
            show_menu(bot, user)
            return HttpResponse(status=200)

        return HttpResponse(status=200)

    if text == 'Органайзер':
        show_organizer(bot, user)
        return HttpResponse(status=200)

    if user.step in ['organizer','show_charges']:
        if text == 'Назад':
            show_menu(bot, user)
            return HttpResponse(status=200)

        if text in ['Расходы на свадьбу', 'Показать весь список']:
            show_charges(bot, user)
            return HttpResponse(status=200)

        if text == 'Гости на свадьбу':
            show_guests(bot, user)
            return HttpResponse(status=200)

    if user.step in ['guests','show_groom_guests','show_bride_guests']:
        if text == 'Назад':
            show_organizer(bot, user)
            return HttpResponse(status=200)

        if text in ['Сторона невесты', 'Показать гостей невесты']:
            show_bride_guests(bot, user)
            return HttpResponse(status=200)

        if text in ['Сторона жениха', 'Показать гостей жениха']:
            show_groom_guests(bot, user)
            return HttpResponse(status=200)

    if user.step == 'show_bride_guests':
        if text == 'Назад':
            show_guests(bot, user)
            return HttpResponse(status=200)

        if text == 'Добавить':
            bot.send_message(
                chat_id=chat_id,
                text='Введите Ваших гостей',
                reply_markup=ReplyKeyboardRemove()
            )

            user.step = 'input_bride_guests_append'
            user.save()

            return HttpResponse(status=200)

        if text == 'Изменить весь список':
            bot.send_message(
                chat_id=user.chat_id,
                text='Введите Ваших гостей',
                reply_markup=ReplyKeyboardRemove()
            )

            user.step = f'input_bride_guests'
            user.save()

            return HttpResponse(status=200)

    if user.step == 'show_groom_guests':
        if text == 'Назад':
            show_guests(bot, user)
            return HttpResponse(status=200)

        if text == 'Добавить':
            bot.send_message(
                chat_id=chat_id,
                text='Введите Ваших гостей',
                reply_markup=ReplyKeyboardRemove()
            )

            user.step = 'input_groom_guests_append'
            user.save()

            return HttpResponse(status=200)

        if text == 'Изменить весь список':
            bot.send_message(
                chat_id=user.chat_id,
                text='Введите Ваших гостей',
                reply_markup=ReplyKeyboardRemove()
            )

            user.step = f'input_groom_guests'
            user.save()

            return HttpResponse(status=200)

        return HttpResponse(status=200)

    if user.step.startswith('input_bride_guests'):
        bride_guests_text = text.strip()

        if not bride_guests_text:
            bot.send_message(
                chat_id=user.chat_id,
                text='Введите гостей со стороны невесты'
            )
            return HttpResponse(status=200)

        # if user.step.endswith('append'):
        #     user.brideguests.text = f'{user.brideguests.text}\n' \
        #                             f'{bride_guests_text}'
        #     user.brideguests.save()
        #
        #     show_bride_guests(bot, user)
        #     return HttpResponse(status=200)

        #charges, created = BrideGuests.objects.update_or_create(
        BrideGuests.objects.create(
            user=user,
            text= bride_guests_text
        )

        show_bride_guests(bot, user)
        return HttpResponse(status=200)

    if user.step.startswith('input_groom_guests'):
        groom_guests_text = text.strip()

        if not groom_guests_text:
            bot.send_message(
                chat_id=user.chat_id,
                text='Введите гостей со стороны жениха'
            )
            return HttpResponse(status=200)

        # if user.step.endswith('append'):
        #     user.groomguests.text = f'{user.groomguests.text}\n' \
        #                             f'{groom_guests_text}'
        #     user.groomguests.save()
        #
        #     show_groom_guests(bot, user)
        #     return HttpResponse(status=200)

        #charges, created = GroomGuests.objects.update_or_create(
        GroomGuests.objects.create(
            user=user,
            text= groom_guests_text
        )

        show_groom_guests(bot, user)
        return HttpResponse(status=200)

    if user.step == 'show_charges':
        if text == 'Назад':
            show_organizer(bot, user)
            return HttpResponse(status=200)

        if text == 'Добавить':
            bot.send_message(
                chat_id=chat_id,
                text='Введите Ваши расходы',
                reply_markup=ReplyKeyboardRemove()
            )

            user.step = 'input_charges_append'
            user.save()

            return HttpResponse(status=200)

        if text == 'Изменить весь список':
            bot.send_message(
                chat_id=chat_id,
                text='Введите Ваши расходы',
                reply_markup=ReplyKeyboardRemove()
            )

            user.step = 'input_charges'
            user.save()

            return HttpResponse(status=200)

    if user.step.startswith('input_charges'):
        charges_text = text.strip()

        bot.send_message(354691583,'11')

        if not charges_text:
            return HttpResponse(status=200)

        # if user.step.endswith('append'):
        #     user.charges.text = f'{user.charges.text}\n' \
        #                         f'{charges_text}'
        #     user.charges.save()
        #
        #     show_charges(bot, user)
        #     return HttpResponse(status=200)

        #charges, created = Charges.objects.update_or_create(
        Charges.objects.create(
            user=user,
            text=charges_text
        )

        show_charges(bot, user)
        return HttpResponse(status=200)

    if text == 'События и акции':
        promos = Promo.objects.all()

        keyboard = build_keyboard(
            buttons=[promo.title for promo in promos]
        )

        bot.send_message(
            chat_id=chat_id,
            text='Выберите интересующую Вас акцию',
            reply_markup=keyboard
        )

        user.step = 'promos'
        user.save()

        return HttpResponse(status=200)

    if user.step == 'promos':
        if text == 'Назад':
            show_menu(bot, user)
            return HttpResponse(status=200)

        try:
            promo = Promo.objects.get(title__icontains=text)
        except Promo.DoesNotExist:
            bot.send_message(
                chat_id=chat_id,
                text='Указанная акция не найдена! Попробуйте еще раз или вернитесь в главное меню.'
            )
            return HttpResponse(status=200)
        else:
            promo_response_text = f'{promo.title}\n\n' \
                                  f'{promo.text}'
            if promo.photo_file_id:
                try:
                    bot.send_photo(
                        chat_id=chat_id,
                        photo=promo.photo_file_id,
                        caption=promo_response_text
                    )
                # if file_id is expired or incorrect - upload image itself
                # and save file_id to database
                except telebot.apihelper.ApiException:
                    print(f'Promo: {promo} - photo_file_id is incorrect')
                    send_photo_result = bot.send_photo(
                        chat_id=chat_id,
                        photo=promo.photo,
                        caption=promo_response_text
                    )
                    promo.photo_file_id = send_photo_result.photo[-1].file_id
                    promo.save()
                else:
                    print(f'Promo: {promo} - photo_file_id is correct, uploading using file_id')
            # if it does not have file_id - upload image itself and save it to database
            else:
                print(f'Promo: {promo} - no photo_file_id, uploading photo')
                try:
                    send_photo_result = bot.send_photo(
                        chat_id,
                        photo=promo.photo,
                        caption=promo_response_text
                    )
                except ApiException:
                    print(f'Promo: {promo} - Incorrect format / size / dimensions of file.')
                else:
                    promo.photo_file_id = send_photo_result.photo[-1].file_id
                    promo.save()
        return HttpResponse(status=200)

    if text == 'Обзоры':
        reviews = Review.objects.all()

        keyboard = build_keyboard(
            buttons=[review.title for review in reviews]
        )

        bot.send_message(
            chat_id=chat_id,
            text='Выберите интересующий Вас обзор',
            reply_markup=keyboard
        )

        user.step = 'reviews'
        user.save()

        return HttpResponse(status=200)

    if user.step == 'reviews':
        if text == 'Назад':
            show_menu(bot, user)
            return HttpResponse(status=200)

        try:
            review = Review.objects.get(title__icontains=text)
        except Review.DoesNotExist:
            bot.send_message(
                chat_id=chat_id,
                text='Указанный обзор не найден! Попробуйте еще раз или вернитесь в главное меню.'
            )
            return HttpResponse(status=200)
        else:
            promo_response_text = f'{review.title}\n\n' \
                                  f'{review.text}'
            if review.photo_file_id:
                try:
                    bot.send_photo(
                        chat_id=chat_id,
                        photo=review.photo_file_id,
                        caption=promo_response_text
                    )
                # if file_id is expired or incorrect - upload image itself
                # and save file_id to database
                except telebot.apihelper.ApiException:
                    print(f'Promo: {review} - photo_file_id is incorrect')
                    send_photo_result = bot.send_photo(
                        chat_id=chat_id,
                        photo=review.photo,
                        caption=promo_response_text
                    )
                    review.photo_file_id = send_photo_result.photo[-1].file_id
                    review.save()
                else:
                    print(f'Promo: {review} - photo_file_id is correct, uploading using file_id')
            # if it does not have file_id - upload image itself and save it to database
            else:
                print(f'Promo: {review} - no photo_file_id, uploading photo')
                try:
                    send_photo_result = bot.send_photo(
                        chat_id,
                        photo=review.photo,
                        caption=promo_response_text
                    )
                except ApiException:
                    print(f'Promo: {review} - Incorrect format / size / dimensions of file.')
                else:
                    review.photo_file_id = send_photo_result.photo[-1].file_id
                    review.save()
        return HttpResponse(status=200)

    if user.step == 'favorite_specialists':
        if text == 'Назад':
            show_personal_information(bot, user)
            return HttpResponse(status=200)

        specialist = get_specialist_by_name(text)
        if specialist:
            decrement_and_notify(bot, config, specialist)
            show_specialist(bot, user, specialist)
            user.step = f'{user.step}/{specialist.id}_specialist'
            user.save()
        return HttpResponse(status=200)

    if text == 'Поиск':
        bot.send_message(
            chat_id=chat_id,
            text='Введите Ваш запрос',
            reply_markup=ReplyKeyboardRemove()
        )

        user.step = 'search'
        user.save()

        return HttpResponse(status=200)

    if user.step == 'search':
        query = text.strip()
        if not query:
            bot.send_message(
                chat_id=user.chat_id,
                text=text
            )
        else:
            show_specialists_by_query(bot, user, query)

        return HttpResponse(status=200)

    if user.step.endswith('query'):
        if text == 'Назад':
            show_menu(bot, user)
            return HttpResponse(status=200)

        specialist = get_specialist_by_name(text)
        if specialist:
            decrement_and_notify(bot, config, specialist)
            show_specialist(bot, user, specialist)

            user.step = f'{user.step}/{specialist.id}_specialist'
            user.save()

        return HttpResponse(status=200)

    if text == 'Специалисты':
        show_categories(bot, user)
        return HttpResponse(status=200)

    if user.step == 'categories_list':
        if text == 'Назад':
            show_menu(bot, user)
            return HttpResponse(status=200)

        try:
            category = Category.objects.get(name=text)
        except Category.DoesNotExist:
            bot.send_message(
                chat_id=chat_id,
                text='Категория с таким названием не найдена. Убедитесь, что вы выбрали категорию из меню!'
            )
        else:
            show_specialists_by_category(bot, user, category)

        return HttpResponse(status=200)

    if user.step.endswith('category'):
        if text == 'Назад':
            show_categories(bot, user)
            return HttpResponse(status=200)

        category_pk = user.step.split('/')[1].split('_', 1)[0]

        specialist = get_specialist_by_name(text)
        if not specialist:
            bot.send_message(
                chat_id=chat_id,
                text='Ошибка: специалист недоступен для просмотра!'
            )
            show_menu(bot, user)
            return HttpResponse(status=200)

        decrement_and_notify(bot, config, specialist)
        show_specialist(bot, user, specialist)

        user.step = f'categories_list/{category_pk}_category/{specialist.pk}_specialist'
        user.save()

        return HttpResponse(status=200)

    if user.step.endswith('specialist'):
        specialist_pk = re.search(r'(?P<id>\d+)_specialist', user.step).group('id')

        try:
            specialist = Specialist.objects.with_views().get(pk=specialist_pk)
        except Specialist.DoesNotExist:
            bot.send_message(
                chat_id=chat_id,
                text='Ошибка: специалист недоступен для просмотра!'
            )
            show_menu(bot, user)
            return HttpResponse(status=200)

        prev_path = '/'.join(user.step.split('/')[:-1])

        if text == 'Назад':
            if '_category' in prev_path:
                category_pk = user.step.split('/')[1].split('_', 1)[0]
                try:
                    category = Category.objects.get(pk=category_pk)
                except Category.DoesNotExist:
                    keyboard = get_main_menu()
                    bot.send_message(
                        chat_id=chat_id,
                        text='Ошибка: не получилось получить категорию специалиста. Вы будете перенаправлены в главное меню',
                        reply_markup=keyboard
                    )
                else:
                    show_specialists_by_category(bot, user, category)
                return HttpResponse(status=200)

            elif 'favorite_specialists' in prev_path:
                show_favorite_specialists(bot, user)
                return HttpResponse(status=200)

            elif 'query' in prev_path:
                query = re.search(r'(?P<query>[\w\d\s]+)_query', user.step).group('query')
                show_specialists_by_query(bot, user, query)

                user.step = get_prev_step(user)
                user.save()

                return HttpResponse(status=200)

            else:
                return HttpResponse(status=200)

        if text == 'Галерея':
            exhibits = specialist.exhibit_set.exclude(Q(photo__exact='') & Q(video_url__exact=''))
            if not exhibits.exists():
                bot.send_message(
                    chat_id=user.chat_id,
                    text='У этого пользователя в галерее ничего нет.'
                )
            else:
                keyboard = build_keyboard(
                    buttons=['Еще']
                )

                bot.send_message(
                    chat_id=chat_id,
                    text='Галерея',
                    reply_markup=keyboard
                )

                show_specialist_exhibit(bot, user, specialist, exhibits)
                user.step = f'{user.step}/gallery'
                user.save()

            return HttpResponse(status=200)

        if text == 'Стоимость':
            bot.send_message(
                chat_id=chat_id,
                text=specialist.pricing,
                parse_mode='Markdown'
            )

        if text == 'Информация':
            bot.send_message(
                chat_id=chat_id,
                text=specialist.information,
                parse_mode='Markdown'
            )

        if text == 'Контакты':
            bot.send_message(
                chat_id=chat_id,
                text=specialist.contacts,
                parse_mode='Markdown'
            )

        if text == '⭐️ Добавить в закладки':
            if specialist in user.favorite_specialists.with_views().all():
                text = f'Специалист *{specialist.name}* уже у Вас в закладках!'
            else:
                user.favorite_specialists.add(specialist)
                text = f'Специалист *{specialist.name}* *добавлен* в закладки.'

            bot.send_message(
                chat_id=chat_id,
                text=text,
                reply_markup=build_specialist_keyboard(user, specialist),
                parse_mode='Markdown'
            )

        if text == '🚫 Удалить из закладок':
            if specialist not in user.favorite_specialists.with_views().all():
                text = f'Специалист *{specialist.name}* *не найден* в Ваших закладках!'
            else:
                user.favorite_specialists.remove(specialist)
                text = f'Специалист *{specialist.name}* *удален* из Ваши закладок.'

            bot.send_message(
                chat_id=chat_id,
                text=text,
                reply_markup=build_specialist_keyboard(user, specialist),
                parse_mode='Markdown'
            )

        if text == 'Рейтинг':
            user.step = f'{user.step}/rating'
            user.save()

            votes = specialist.votes.aggregate(
                positive=Count(
                    'status',
                    filter=Q(status=Vote.VOTE_POSITIVE)
                ),
                negative=Count(
                    'status',
                    filter=Q(status=Vote.VOTE_NEGATIVE)
                )
            )

            keyboard = build_keyboard(
                buttons=[
                    '👍🏻 Положительный', '👎🏻 Отрицательный'
                ]
            )

            bot.send_message(
                chat_id=chat_id,
                text=f'Рейтинг специалиста "*{specialist.name}*"\n\n'
                     f'Положительных: *{votes["positive"]}*\n'
                     f'Отрицательных: *{votes["negative"]}*\n\n'
                     f'Отдайте Ваш голос за или против специалиста.',
                reply_markup=keyboard,
                parse_mode='Markdown'
            )

        if text == 'Отзывы':
            comments = specialist.comments.all()

            if comments.exists():
                text = '\n\n'.join(
                    f'{comment.text}\n'
                    f'*от:* _{comment.user.get_name}_'
                    for comment in comments
                )
            else:
                text = 'Никто из пользователей еще не оставил отзыв об этом специалисте.'

            keyboard = build_keyboard(
                buttons=[
                    'Отправить отзыв'
                ]
            )

            bot.send_message(
                chat_id=chat_id,
                text=text,
                reply_markup=keyboard,
                parse_mode='Markdown'
            )

            user.step = f'{user.step}/comments'
            user.save()

        return HttpResponse(status=200)

    if user.step.endswith('rating'):
        specialist_pk = re.search(r'(?P<id>\d+)_specialist', user.step).group('id')

        try:
            specialist = Specialist.objects.with_views().get(pk=specialist_pk)
        except Specialist.DoesNotExist:
            keyboard = get_main_menu()
            bot.send_message(
                chat_id=chat_id,
                text='Ошибка: не получилось получить специалиста. Вы будете перенаправлены в главное меню',
                reply_markup=keyboard
            )
        else:
            if text == 'Назад':
                show_specialist(bot, user, specialist)

                user.step = get_prev_step(user)
                user.save()

                return HttpResponse(status=200)

            user_vote, created = user.votes.get_or_create(
                specialist=specialist,
                defaults={
                    'status': Vote.VOTE_POSITIVE if text == '👍🏻 Положительный' else Vote.VOTE_NEGATIVE
                }
            )

            if created:
                text = f'Ваш "{user_vote.get_status_display()}" голос *сохранен*.'
            else:
                if user_vote.get_status_display() == text:
                    user_vote.delete()
                    text = f'Вы *забрали* свой "{text}" голос.'
                else:
                    old_status = user_vote.get_status_display()

                    user_vote.status = Vote.VOTE_POSITIVE if text == '👍🏻 Положительный' else Vote.VOTE_NEGATIVE
                    user_vote.save()

                    text = f'Ваш голос *изменен* с "{old_status}" на "{user_vote.get_status_display()}".'

            bot.send_message(
                chat_id=chat_id,
                text=text,
                parse_mode='Markdown'
            )

            votes = specialist.votes.aggregate(
                positive=Count(
                    'status',
                    filter=Q(status=Vote.VOTE_POSITIVE)
                ),
                negative=Count(
                    'status',
                    filter=Q(status=Vote.VOTE_NEGATIVE)
                )
            )

            bot.send_message(
                chat_id=chat_id,
                text=f'Рейтинг специалиста "*{specialist.name}*"\n\n'
                     f'Положительных: *{votes["positive"]}*\n'
                     f'Отрицательных: *{votes["negative"]}*',
                parse_mode='Markdown'
            )

        return HttpResponse(status=200)

    if user.step.endswith('gallery'):
        specialist_pk = re.search(r'(?P<id>\d+)_specialist', user.step).group('id')

        try:
            specialist = Specialist.objects.with_views().get(pk=specialist_pk)
        except Specialist.DoesNotExist:
            keyboard = get_main_menu()
            bot.send_message(
                chat_id=chat_id,
                text='Ошибка: не получилось получить специалиста. Вы будете перенаправлены в главное меню',
                reply_markup=keyboard
            )
            return HttpResponse(status=200)
        else:
            if text == 'Назад':
                show_specialist(bot, user, specialist)

                user.step = get_prev_step(user)
                user.save()

                return HttpResponse(status=200)

            if text == 'Еще':
                exhibits = specialist.exhibit_set.exclude(Q(photo__exact='') & Q(video_url__exact=''))
                if not exhibits.exists():
                    bot.send_message(
                        chat_id=user.chat_id,
                        text='У этого пользователя в галерее ничего нет.'
                    )
                else:
                    show_specialist_exhibit(bot, user, specialist, exhibits)

                return HttpResponse(status=200)

    if user.step.endswith('comments'):
        specialist_pk = re.search(r'(?P<id>\d+)_specialist', user.step).group('id')

        try:
            specialist = Specialist.objects.with_views().get(pk=specialist_pk)
        except Specialist.DoesNotExist:
            keyboard = get_main_menu()
            bot.send_message(
                chat_id=chat_id,
                text='Ошибка: не получилось получить специалиста. Вы будете перенаправлены в главное меню',
                reply_markup=keyboard
            )
        else:
            if text == 'Назад':
                show_specialist(bot, user, specialist)

                user.step = get_prev_step(user)
                user.save()

                return HttpResponse(status=200)

            if text == 'Отправить отзыв':
                bot.send_message(
                    chat_id=chat_id,
                    text=f'Напишите Ваш отзыв о специалисте: *{specialist.name}*',
                    reply_markup=ReplyKeyboardRemove(),
                    parse_mode='Markdown'
                )

                user.step = f'{user.step}/new'
                user.save()

            return HttpResponse(status=200)

    if user.step.endswith('comments/new'):
        specialist_pk = re.search(r'(?P<id>\d+)_specialist', user.step).group('id')

        try:
            specialist = Specialist.objects.with_views().get(pk=specialist_pk)
        except Specialist.DoesNotExist:
            keyboard = get_main_menu()
            bot.send_message(
                chat_id=chat_id,
                text='Ошибка: не получилось получить специалиста. Вы будете перенаправлены в главное меню',
                reply_markup=keyboard
            )
        else:
            comment_text = text.strip()

            if not comment_text:
                bot.send_message(
                    chat_id=chat_id,
                    text='Введите текст Вашего отзыва!',
                )
            else:
                user.comments.create(
                    specialist=specialist,
                    text=comment_text
                )

                bot.send_message(
                    chat_id=chat_id,
                    text=f'Ваш отзыв о специалисте *{specialist.name}* сохранен.',
                    parse_mode='Markdown'
                )

                comments = specialist.comments.all()

                if comments.exists():
                    text = '\n\n'.join(
                        f'{comment.text}\n'
                        f'*от:* _{comment.user.get_name}_'
                        for comment in comments
                    )
                else:
                    text = 'Никто из пользователей еще не оставил отзыв об этом специалисте.'

                keyboard = build_keyboard(
                    buttons=[
                        'Отправить отзыв'
                    ]
                )

                bot.send_message(
                    chat_id=chat_id,
                    text=text,
                    reply_markup=keyboard,
                    parse_mode='Markdown'
                )

                user.step = get_prev_step(user)
                user.save()

        return HttpResponse(status=200)

    # end of bot view
    return HttpResponse(status=200)


def get_bot_user(user_dict):
    user, _ = BotUser.objects.get_or_create(
        chat_id=user_dict['id'],
        defaults={
            'username': user_dict.get('username', '')
        })

    return user


def get_main_menu():
    keyboard = build_keyboard(
        buttons=MAIN_MENU,
        disable_navigation=True
    )
    return keyboard


def build_keyboard(buttons, rows_pre=None, rows_post=None, disable_navigation=None, resize_keyboard=True,
                   one_time_keyboard=False):
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard, one_time_keyboard)

    if rows_pre:
        for row in rows_pre:
            keyboard.row(*row)

    keyboard.add(*buttons)

    if rows_post:
        for row in rows_post:
            keyboard.row(*row)

    if not disable_navigation:
        keyboard.row('Меню', 'Назад')

    return keyboard


def build_specialist_keyboard(user, specialist):
    keyboard = build_keyboard(
        buttons=[
            'Галерея', 'Стоимость',
            'Информация', 'Контакты',
            'Рейтинг', 'Отзывы',

            '⭐️ Добавить в закладки' if specialist not in user.favorite_specialists.with_views() else '🚫 Удалить из закладок'
        ]
    )

    return keyboard


def show_specialist(bot, user, specialist):
    keyboard = build_specialist_keyboard(user, specialist)

    if specialist.photo_file_id:
        photo = specialist.photo_file_id
    else:
        photo = specialist.photo

    send_photo_result = bot.send_photo(
        chat_id=user.chat_id,
        photo=photo,
        caption=specialist.name,
        reply_markup=keyboard
    )

    if not specialist.photo_file_id:
        specialist.photo_file_id = send_photo_result.photo[-1].file_id
        specialist.save()


def show_categories(bot, user):
    categories = Category.objects.all()

    keyboard = build_keyboard(
        buttons=[category.name for category in categories]
    )

    bot.send_message(
        chat_id=user.chat_id,
        text='Выберите интересующую Вас категорию специалистов:',
        reply_markup=keyboard
    )

    user.step = 'categories_list'
    user.save()


def show_menu(bot, user):
    keyboard = get_main_menu()

    bot.send_message(
        chat_id=user.chat_id,
        text='Меню',
        reply_markup=keyboard
    )

    user.step = ''
    user.save()


def build_specialists_keyboard(user, specialists_qs):
    keyboard = build_keyboard(
        buttons=[
            f'{"⭐️ " if specialist in user.favorite_specialists.with_views() else ""}{specialist.name}'
            for specialist in specialists_qs
        ]
    )
    return keyboard


def show_favorite_specialists(bot, user):
    specialists = user.favorite_specialists.with_views().filter(cities__id=user.city_id)
    if not specialists.exists():
        bot.send_message(
            chat_id=user.chat_id,
            text='По Вашему запросу не найдено ни одного специалиста.',
        )
    else:
        keyboard = build_specialists_keyboard(user, specialists)

        bot.send_message(
            chat_id=user.chat_id,
            text='Выберите интересующего Вас специалиста:',
            reply_markup=keyboard
        )

        user.step = 'favorite_specialists'
        user.save()


def show_specialists_by_query(bot, user, query):
    specialists = Specialist.objects.with_views().filter(cities__id=user.city_id).filter(name__icontains=query)
    if not specialists.exists():
        bot.send_message(
            chat_id=user.chat_id,
            text='По Вашему запросу не найдено ни одного специалиста.',
        )
        show_menu(bot, user)
    else:
        keyboard = build_specialists_keyboard(user, specialists)

        bot.send_message(
            chat_id=user.chat_id,
            text='Выберите интересующего Вас специалиста:',
            reply_markup=keyboard
        )


def show_specialists_by_category(bot, user, category):
    specialists = category.specialists.with_views().filter(cities__id=user.city_id)
    if not specialists.exists():
        bot.send_message(
            chat_id=user.chat_id,
            text='По Вашему запросу не найдено ни одного специалиста.',
        )
    else:
        keyboard = build_specialists_keyboard(user, specialists)

        bot.send_message(
            chat_id=user.chat_id,
            text='Выберите интересующего Вас специалиста:',
            reply_markup=keyboard
        )

        user.step = f'categories_list/{category.pk}_category'
        user.save()


def get_specialist_by_name(name):
    if name.startswith('⭐️'):
        name = name.split(maxsplit=1)[1]

    try:
        specialist = Specialist.objects.with_views().get(name=name)
    except Specialist.DoesNotExist:
        print(f'Specialist with name "{name}" was not found.')
        return None
    else:
        return specialist


def show_charges(bot, user):



    # try:
    #     charges_text = user.charges
    # except Charges.DoesNotExist:
    #     bot.send_message(
    #         chat_id=user.chat_id,
    #         text='Введите Ваши расходы',
    #         reply_markup=ReplyKeyboardRemove()
    #     )
    #
    #     user.step = 'input_charges'
    #     user.save()
    #
    # else:
    #     keyboard = build_keyboard(
    #         buttons=[
    #             'Добавить',
    #             'Изменить весь список'
    #         ]
    #     )
    #
    #     bot.send_message(
    #         chat_id=user.chat_id,
    #         text=f'💰 Ваши расходы на свадьбу\n\n'
    #              f'{charges_text.text}',
    #         reply_markup=keyboard
    #     )
    #
    #     user.step = 'show_charges'
    #     user.save()

    charges = Charges.objects.filter(user=user)

    if charges.count() == 0:
        bot.send_message(
            chat_id=user.chat_id,
            text='Введите Ваши расходы',
            reply_markup=ReplyKeyboardRemove()
        )

        user.step = 'input_charges'
        user.save()
    else:
        keyboard = build_keyboard(
            buttons=[
                'Добавить',
                #'Изменить весь список'
                'Показать весь список'
            ]
        )

        bot.send_message(
            chat_id=user.chat_id,
            text='💰 Ваши расходы на свадьбу\n\n',
            reply_markup=keyboard
        )



        for charge in charges:
            mes=charge.text
            keyboard = telebot.types.InlineKeyboardMarkup()
            keyboard.add(telebot.types.InlineKeyboardButton(text='Удалить', callback_data='delete_charge'+str(charge.id)))
            keyboard.add(telebot.types.InlineKeyboardButton(text='Редактировать', callback_data='edit_charge'+str(charge.id)))
            bot.send_message(
                chat_id=user.chat_id,
                text=mes,
                reply_markup=keyboard
            )

        user.step = 'show_charges'
        user.save()


def show_organizer(bot, user):
    keyboard = build_keyboard(
        buttons=[
            'Расходы на свадьбу',
            'Гости на свадьбу'
        ]
    )

    bot.send_message(
        chat_id=user.chat_id,
        text='Выберите интересующий Вас раздел',
        reply_markup=keyboard
    )

    user.step = 'organizer'
    user.save()


def show_bride_guests(bot, user):
    # try:
    #     guests_text = user.brideguests.text
    # except BrideGuests.DoesNotExist:
    #     bot.send_message(
    #         chat_id=user.chat_id,
    #         text='Введите Ваших гостей',
    #         reply_markup=ReplyKeyboardRemove()
    #     )
    #
    #     user.step = f'input_bride_guests'
    #     user.save()
    # else:
    #     keyboard = build_keyboard(
    #         buttons=[
    #             'Добавить',
    #             'Изменить весь список'
    #         ]
    #     )
    #
    #     bot.send_message(
    #         chat_id=user.chat_id,
    #         text=f'👰 Гости со стороны невесты\n\n' \
    #              f'{guests_text}',
    #         reply_markup=keyboard
    #     )
    #
    #     user.step = 'show_bride_guests'
    #     user.save()

    bride_guests = BrideGuests.objects.filter(user=user)

    if bride_guests.count() == 0:
        bot.send_message(
            chat_id=user.chat_id,
            text='Введите Ваших гостей',
            reply_markup=ReplyKeyboardRemove()
        )

        user.step = 'input_bride_guests'
        user.save()
    else:
        keyboard = build_keyboard(
            buttons=[
                'Добавить',
                #'Изменить весь список'
                'Показать гостей невесты'
            ]
        )

        bot.send_message(
            chat_id=user.chat_id,
            text='👰 Гости со стороны невесты',
            reply_markup=keyboard
        )

        for bride_guest in bride_guests:
            mes=bride_guest.text
            keyboard = telebot.types.InlineKeyboardMarkup()
            keyboard.add(telebot.types.InlineKeyboardButton(text='Удалить', callback_data='delete_bride_guest'+str(bride_guest.id)))
            keyboard.add(telebot.types.InlineKeyboardButton(text='Редактировать', callback_data='edit_bride_guest'+str(bride_guest.id)))
            bot.send_message(
                chat_id=user.chat_id,
                text=mes,
                reply_markup=keyboard
            )

        user.step = 'show_bride_guests'
        user.save()


def show_groom_guests(bot, user):

    # try:
    #     guests_text = user.groomguests.text
    # except GroomGuests.DoesNotExist:
    #     bot.send_message(
    #         chat_id=user.chat_id,
    #         text='Введите Ваших гостей',
    #         reply_markup=ReplyKeyboardRemove()
    #     )
    #
    #     user.step = f'input_groom_guests'
    #     user.save()
    # else:
    #     keyboard = build_keyboard(
    #         buttons=[
    #             'Добавить',
    #             'Изменить весь список'
    #         ]
    #     )
    #
    #     bot.send_message(
    #         chat_id=user.chat_id,
    #         text=f'🤵 Гости со стороны жениха\n\n'
    #              f'{guests_text}',
    #         reply_markup=keyboard
    #     )
    #
    #     user.step = 'show_groom_guests'
    #     user.save()

    groom_guests = GroomGuests.objects.filter(user=user)

    if groom_guests.count() == 0:
        bot.send_message(
            chat_id=user.chat_id,
            text='Введите Ваших гостей',
            reply_markup=ReplyKeyboardRemove()
        )

        user.step = 'input_groom_guests'
        user.save()
    else:
        keyboard = build_keyboard(
            buttons=[
                'Добавить',
                #'Изменить весь список'
                'Показать гостей жениха'
            ]
        )

        bot.send_message(
            chat_id=user.chat_id,
            text='🤵 Гости со стороны жениха',
            reply_markup=keyboard
        )

        for groom_guest in groom_guests:
            mes=groom_guest.text
            keyboard = telebot.types.InlineKeyboardMarkup()
            keyboard.add(telebot.types.InlineKeyboardButton(text='Удалить', callback_data='delete_groom_guest'+str(groom_guest.id)))
            keyboard.add(telebot.types.InlineKeyboardButton(text='Редактировать', callback_data='edit_groom_guest'+str(groom_guest.id)))
            bot.send_message(
                chat_id=user.chat_id,
                text=mes,
                reply_markup=keyboard
            )

        user.step = 'show_groom_guests'
        user.save()


def show_guests(bot, user):
    keyboard = build_keyboard(
        buttons=[
            'Сторона невесты',
            'Сторона жениха'
        ]
    )

    bot.send_message(
        chat_id=user.chat_id,
        text='Выберите интересующий Вас раздел',
        reply_markup=keyboard
    )

    user.step = 'guests'
    user.save()


def decrement_and_notify(bot, config, specialist):
    with transaction.atomic():
        specialist.allowed_views = F('allowed_views') - 1
        specialist.save()

    specialist.refresh_from_db()

    if specialist.allowed_views == 0:
        recipients = config.notify_about_out_of_views
        if recipients:
            for recipient in recipients.all():
                try:
                    bot.send_message(
                        chat_id=recipient.chat_id,
                        text=f'👁 *Просмотры закончились*\n'
                             f'[👉🏻 Открыть специалиста "{specialist.name}"]({settings.DOMAIN}{specialist.get_admin_url})',
                        parse_mode='Markdown'
                    )
                except ApiException:
                    print(f'Couldn\'t send message to user with ID "{recipient.chat_id}" ')


def show_specialist_exhibit(bot, user, specialist, exhibits):
    exhibit_id = specialist.current_exhibit_id

    exhibit = exhibits[exhibit_id]

    exhibit_id = (exhibit_id + 1) if not (exhibit_id + 1) > (exhibits.count() - 1) else 0

    with transaction.atomic():
        specialist.current_exhibit_id = exhibit_id
        specialist.save()

    # photo
    upload_photo_file = False

    if exhibit.photo_file_id:
        try:
            bot.send_photo(
                chat_id=user.chat_id,
                photo=exhibit.photo_file_id
            )
            print('sending photo with id')
        except ApiException:
            upload_photo_file = True
        except Exception as e:
            print(f'Exhibit: {exhibit} - exception during send photo: "{str(e)}"')
    else:
        upload_photo_file = True

    if upload_photo_file and exhibit.photo:
        try:
            send_photo_result = bot.send_photo(
                chat_id=user.chat_id,
                photo=exhibit.photo
            )
        except ApiException:
            print(f'Exhibit: {exhibit} - Incorrect format / size / dimensions of file.')
        except Exception as e:
            print(f'Exhibit: {exhibit} - exception during send photo: "{str(e)}"')
        else:
            exhibit.photo_file_id = send_photo_result.photo[-1].file_id
            exhibit.save()

    # video
    if exhibit.video_url:
        bot.send_message(
            chat_id=user.chat_id,
            text=exhibit.video_url
        )



def get_prev_step(user):
    return '/'.join(user.step.split('/')[:-1])


def ask_for_name(bot, user):
    bot.send_message(
        chat_id=user.chat_id,
        text='Введите Ваше имя:',
        reply_markup=ReplyKeyboardRemove()
    )

    user.step = 'input_name'
    user.save()


def ask_for_city(bot, user):
    cities = City.objects.all()

    keyboard = build_keyboard(
        rows_post=[
            ['Указать позже']
        ],
        buttons=[city.name for city in cities],
        disable_navigation=True
    )

    bot.send_message(
        chat_id=user.chat_id,
        text='Выберите Ваш город.\n\n'
             'Если Вашего города нет в списке - выберите "*Указать позже*"\n\n'
             'Если город не указан - Вы не сможете просматривать специалистов.',
        reply_markup=keyboard,
        parse_mode='Markdown'
    )

    user.step = 'input_city'
    user.save()
