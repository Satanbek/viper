from datetime import datetime

import telebot

from . import init_bot, run_bot
from .models import BotUser

bot = init_bot()


# logic

def build_menu(items):
    keyboard = telebot.types.ReplyKeyboardMarkup(resize_keyboard=True, one_time_keyboard=False)
    keyboard.add(*items)

    return keyboard


def get_bot_user(message):
    user = message.from_user

    bot_user, _ = BotUser.objects.get_or_create(
        chat_id=message.from_user.id,
        defaults={
            'username': user.username,
        })

    return bot_user


def set_name(message, name):
    bot_user = get_bot_user(message)
    bot_user.name = name
    bot_user.save()


def set_wedding_date(message, date):
    bot_user = get_bot_user(message)
    bot_user.wedding_date = date
    bot_user.save()


def set_pair_photo(message, photo, file_id):
    bot_user = get_bot_user(message)
    bot_user.pair_photo = photo
    bot_user.pair_photo_file_id = file_id
    bot_user.save()


# handlers

@bot.message_handler(commands=['start'])
def start_handler(message):
    reply_message = bot.send_message(
        chat_id=message.from_user.id,
        text='Здравствуйте! Вас приветствует бот.\n\n'
             'Введите ваше имя:'
    )
    bot.register_next_step_handler(reply_message, ask_name_handler)


def ask_name_handler(message):
    name = message.text

    if name:
        set_name(message, name)

        reply_message = bot.send_message(
            chat_id=message.from_user.id,
            text='Введите дату свадьбы (в формате `01.01.2001`):',
            parse_mode='Markdown'
        )
        bot.register_next_step_handler(reply_message, ask_wedding_date_handler)
    else:
        reply_message = bot.send_message(
            chat_id=message.from_user.id,
            text='Введите Ваше имя, чтобы продолжить!',
        )
        bot.register_next_step_handler(reply_message, ask_name_handler)


def ask_wedding_date_handler(message):
    try:
        date = datetime.strptime(message.text.strip(), '%d.%m.%Y')
    except ValueError as e:
        reply_message = bot.send_message(
            chat_id=message.from_user.id,
            text='Неверный формат! Убедитесь, что Вы ввели дату как в примере: `01.01.2001`',
            parse_mode='Markdown'
        )
        bot.register_next_step_handler(reply_message, ask_wedding_date_handler)
    else:
        set_wedding_date(message, date)

        reply_message = bot.send_message(
            chat_id=message.from_user.id,
            text='Отправьте фотографию Вашей пары:'
        )
        bot.register_next_step_handler(reply_message, ask_pair_photo)


def ask_pair_photo(message):
    if message.photo:
        photo_size_obj = message.photo[-1]
        file_id = photo_size_obj.file_id

        #
        # file_info = bot.get_file(file_id)
        # photo = bot.download_file(file_info.file_path)

        set_pair_photo(message, None, file_id)

        main_menu_handler(message)
    else:
        reply_message = bot.send_message(
            chat_id=message.from_user.id,
            text='Отправьте фото в чат, чтобы продолжить!'
        )
        bot.register_next_step_handler(reply_message, ask_pair_photo)


@bot.message_handler(commands=['Меню'])
def main_menu_handler(message):
    bot_user = get_bot_user(message)

    items = (
        'Личная информация', 'Специалисты',
        'Избранное', 'Поиск',
        'Обзоры', 'События и акции',
        'Органайзер', 'Обратная связь',
        'Заявка на размещение'
    )

    menu = build_menu(items)

    bot.send_message(
        chat_id=message.from_user.id,
        text=f'Здравствуйте, {bot_user.name}, выберите интересующий Вас раздел',
        reply_markup=menu
    )


run_bot(bot)
