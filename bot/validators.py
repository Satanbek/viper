from django.core.exceptions import ValidationError


def telegram_video_file_size(value):
    max_mb = 50
    limit = max_mb * 1024 * 1024
    if value.size > limit:
        raise ValidationError(f'File too large. Size should not exceed {max_mb} MB.')
