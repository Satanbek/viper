import telebot
from django.conf import settings
from django.db import models
from django.db.models import Count, Q, F
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse

from solo.models import SingletonModel
from telebot.apihelper import ApiException

from .validators import telegram_video_file_size
from .utils import upload_to


class BotConfiguration(SingletonModel):
    feedback_message = models.TextField(blank=True, verbose_name='Ответ на "Обратная связь"')
    start_message = models.TextField(blank=True, verbose_name='Ответ на "/start"')

    notify_about_feedback = models.ManyToManyField(
        'BotUser',
        verbose_name='Оповещения о "Сообщениях обратной связи"',
        related_name='notify_feedback',
        blank=True
    )

    notify_about_specialist_application = models.ManyToManyField(
        'BotUser',
        verbose_name='Оповещения о "Заявках на размещение"',
        related_name='notify_specialist_application',
        blank=True
    )

    notify_about_out_of_views = models.ManyToManyField(
        'BotUser',
        verbose_name='Оповещения об исчерпании "Разрешенных просмотров"',
        related_name='notify_out_of_views',
        blank=True
    )

    def __str__(self):
        return 'Настройки бота'

    class Meta:
        verbose_name = "Настройки бота"


class BotUser(models.Model):
    chat_id = models.BigIntegerField(unique=True)
    username = models.CharField(max_length=128, blank=True, verbose_name='Имя пользователя')

    name = models.CharField(max_length=128, blank=True, verbose_name='Имя')
    city = models.ForeignKey('City', null=True, blank=True, verbose_name='Город', on_delete=models.SET_NULL)

    step = models.CharField(max_length=255, blank=True, verbose_name='Шаг')

    wedding_date = models.DateField(blank=True, null=True, verbose_name='Дата свадьбы')

    pair_photo_file_id = models.CharField(max_length=64, blank=True)

    favorite_specialists = models.ManyToManyField(
        'Specialist',
        related_name='favorite_specialists',
        verbose_name='Избранные специалисты',
        blank=True
    )

    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering = ('created_at',)
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.get_name

    @property
    def get_name(self):
        return self.name or self.username or str(self.id)


class Category(models.Model):
    name = models.CharField(max_length=128, verbose_name='Название')

    class Meta:
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'

    def __str__(self):
        return self.name


class SpecialistManager(models.Manager):
    def get_queryset(self):
        return super(SpecialistManager, self).get_queryset() \
            .prefetch_related('votes') \
            .annotate(
                positive=Count(
                    'votes__status',
                    filter=Q(votes__status=Vote.VOTE_POSITIVE)
                ),
                negative=Count(
                    'votes__status',
                    filter=Q(votes__status=Vote.VOTE_NEGATIVE)
                )
            ) \
            .annotate(rating=F('positive') - F('negative')) \
            .order_by('-inTop', '-rating', 'name')

    def with_views(self):
        return self.filter(allowed_views__gt=0)


class Specialist(models.Model):
    name = models.CharField(max_length=128, verbose_name='Имя')
    photo = models.ImageField(upload_to=upload_to, verbose_name='Фото')
    photo_file_id = models.CharField(max_length=128, blank=True, null=True)

    cities = models.ManyToManyField('City', related_name='specialists', verbose_name='Города')

    allowed_views = models.PositiveIntegerField(default=0, verbose_name='Разрешенные просмотры')

    information = models.TextField(verbose_name='Информация')
    pricing = models.TextField(verbose_name='Стоимость')
    contacts = models.TextField('Контакты')

    inTop = models.BooleanField(default=False, verbose_name='В ТОП')

    current_exhibit_id = models.PositiveIntegerField(default=0)

    categories = models.ManyToManyField('Category', related_name='specialists', verbose_name='Категории')

    objects = SpecialistManager()

    class Meta:
        verbose_name = 'Специалист'
        verbose_name_plural = 'Специалисты'

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.pk is not None:
            prev = Specialist.objects.get(pk=self.pk)

            if prev.photo != self.photo:
                self.photo_file_id = ''

        super(Specialist, self).save(*args, **kwargs)

    @property
    def get_admin_url(self):
        return reverse('admin:{0}_{1}_change'.format(self._meta.app_label, self._meta.model_name), args=(self.pk,))


class Exhibit(models.Model):
    specialist = models.ForeignKey('Specialist', on_delete=models.CASCADE, verbose_name='Специалист')

    photo = models.ImageField(upload_to=upload_to, null=True, blank=True, verbose_name='Фото')
    photo_file_id = models.CharField(max_length=128, blank=True)

    video_url = models.URLField(blank=True, verbose_name='Ссылка на видео')

    class Meta:
        verbose_name = 'Экспонат'
        verbose_name_plural = 'Экспонаты'

    def __str__(self):
        return str(self.id)

    def save(self, *args, **kwargs):
        if self.pk is not None:
            prev = Exhibit.objects.get(pk=self.pk)

            if prev.photo != self.photo:
                self.photo_file_id = ''

        super(Exhibit, self).save(*args, **kwargs)


class Comment(models.Model):
    user = models.ForeignKey(
        'BotUser',
        on_delete=models.CASCADE,
        null=True,
        related_name='comments',
        verbose_name='Пользователи'
    )

    specialist = models.ForeignKey(
        'Specialist',
        on_delete=models.CASCADE,
        related_name='comments',
        verbose_name='Специалист')

    text = models.TextField(verbose_name='Текст')

    created_at = models.DateTimeField(auto_now_add=True, auto_now=False)

    class Meta:
        ordering = ('created_at',)
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'

    def __str__(self):
        return f'Отзыв {self.pk}'


class Review(models.Model):
    title = models.CharField(max_length=64, verbose_name='Название')
    photo = models.ImageField(upload_to=upload_to, verbose_name='Изображение')
    photo_file_id = models.CharField(max_length=128, blank=True)
    text = models.TextField(verbose_name='Текст')

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Обзор'
        verbose_name_plural = 'Обзоры'

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.pk is not None:
            prev = Review.objects.get(pk=self.pk)

            if prev.photo != self.photo:
                self.photo_file_id = ''

        super(Review, self).save(*args, **kwargs)


class Promo(models.Model):
    title = models.CharField(max_length=64, verbose_name='Название')
    photo = models.ImageField(upload_to=upload_to, verbose_name='Изображение')
    photo_file_id = models.CharField(max_length=128, blank=True)
    text = models.TextField(verbose_name='Текст')

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Акция'
        verbose_name_plural = 'Акции'

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if self.pk is not None:
            prev = Promo.objects.get(pk=self.pk)

            if prev.photo != self.photo:
                self.photo_file_id = ''

        super(Promo, self).save(*args, **kwargs)


class Charges(models.Model):
    user = models.ForeignKey(
        'BotUser',
        on_delete=models.CASCADE,
        verbose_name='Расходы'
    )
    text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Расходы'
        verbose_name_plural = 'Расходы'

    def __str__(self):
        return f'Charges{(self.id, self.user)}'


class BrideGuests(models.Model):
    user = models.ForeignKey(
        'BotUser',
        on_delete=models.CASCADE,
        verbose_name='Гости невесты'
    )
    text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Гости невесты'
        verbose_name_plural = 'Гости невесты'


class GroomGuests(models.Model):
    user = models.ForeignKey(
        'BotUser',
        on_delete=models.CASCADE,
        verbose_name='Гости жениха'
    )
    text = models.TextField(verbose_name='Текст')

    class Meta:
        verbose_name = 'Гости жениха'
        verbose_name_plural = 'Гости жениха'


class Vote(models.Model):
    VOTE_POSITIVE = 1
    VOTE_NEGATIVE = 0
    STATUS_CHOICES = (
        (VOTE_POSITIVE, '👍🏻 Положительный'),
        (VOTE_NEGATIVE, '👎🏻 Отрицательный')
    )

    user = models.ForeignKey(
        'BotUser',
        on_delete=models.SET_NULL,
        null=True,
        related_name='votes',
        verbose_name='Пользователи'
    )

    specialist = models.ForeignKey(
        'Specialist',
        on_delete=models.CASCADE,
        related_name='votes',
        verbose_name='Специалист'
    )

    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=VOTE_POSITIVE, verbose_name='Выбор')

    class Meta:
        verbose_name = 'Голос'
        verbose_name_plural = 'Голоса'
        unique_together = ('user', 'specialist')

    def __str__(self):
        return self.get_status_display()


class Feedback(models.Model):
    user = models.ForeignKey(
        'BotUser',
        on_delete=models.CASCADE,
        verbose_name='Пользователь'
    )
    text = models.TextField(verbose_name='Текст')
    is_processed = models.BooleanField(default=False, verbose_name='Обработано?')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата получения')

    class Meta:
        ordering = ('is_processed', '-created_at')
        verbose_name = 'Сообщение обратной связи'
        verbose_name_plural = 'Сообщения обратной связи'

    @property
    def get_admin_url(self):
        return reverse('admin:{0}_{1}_change'.format(self._meta.app_label, self._meta.model_name), args=(self.pk,))


@receiver(post_save, sender=Feedback)
def create_feedback(sender, instance, created, **kwargs):
    if created:
        bot = telebot.TeleBot(settings.TELEGRAM_TOKEN)
        config = BotConfiguration.get_solo()
        recipients = config.notify_about_feedback.all()
        if recipients:
            for recipient in recipients:
                try:
                    bot.send_message(
                        chat_id=recipient.chat_id,
                        text=f'🤖 *Обратная связь*\n'
                             f'от пользователя: [{instance.user.get_name}](tg://user?id={instance.user.chat_id})\n'
                             f'[👉🏻 Открыть]({settings.DOMAIN}{instance.get_admin_url})',
                        parse_mode='Markdown'
                    )
                except ApiException:
                    print(f'Couldn\'t send message to user with ID "{recipient.chat_id}" ')


class SpecialistApplication(models.Model):
    user = models.ForeignKey(
        'BotUser',
        on_delete=models.CASCADE,
        verbose_name='Пользователь'
    )
    text = models.TextField(verbose_name='Текст')
    is_processed = models.BooleanField(default=False, verbose_name='Обработано?')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата получения')

    class Meta:
        ordering = ('is_processed', '-created_at')
        verbose_name = 'Заявка на размещение'
        verbose_name_plural = 'Заявки на размещение'

    @property
    def get_admin_url(self):
        return reverse('admin:{0}_{1}_change'.format(self._meta.app_label, self._meta.model_name), args=(self.pk,))


@receiver(post_save, sender=SpecialistApplication)
def create_specialist_application(sender, instance, created, **kwargs):
    if created:
        bot = telebot.TeleBot(settings.TELEGRAM_TOKEN)
        config = BotConfiguration.get_solo()
        recipients = config.notify_about_specialist_application.all()
        if recipients:
            for recipient in recipients:
                try:
                    bot.send_message(
                        chat_id=recipient.chat_id,
                        text=f'📷 *Заявка на размещение*\n'
                             f'От пользователя: [{instance.user.get_name}](tg://user?id={instance.user.chat_id})\n'
                             f'[👉🏻 Открыть]({settings.DOMAIN}{instance.get_admin_url})',
                        parse_mode='Markdown'
                    )
                except ApiException:
                    print(f'Couldn\'t send message to user with ID "{recipient.chat_id}" ')


class City(models.Model):
    name = models.CharField(max_length=128, verbose_name='Название')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'
