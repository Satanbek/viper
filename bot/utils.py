import uuid

import os

from django.utils import timezone


def generate_filename(filename):
    _, ext = os.path.splitext(filename)
    return f'{uuid.uuid4()}{ext}'


def upload_to(instance, filname):
    fname = generate_filename(filname)
    today = timezone.now().strftime('%Y/%m/%d')
    base_path = 'uploads'

    file_path = os.path.join(base_path, today, fname)

    return file_path
