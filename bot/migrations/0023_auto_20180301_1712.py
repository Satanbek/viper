# Generated by Django 2.0.2 on 2018-03-01 14:12

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0022_auto_20180301_1655'),
    ]

    operations = [
        migrations.CreateModel(
            name='BrideGuests',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(verbose_name='Текст')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='bot.BotUser', verbose_name='Гости невесты')),
            ],
            options={
                'verbose_name': 'Гости невесты',
                'verbose_name_plural': 'Гости невесты',
            },
        ),
        migrations.CreateModel(
            name='GroomGuests',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(verbose_name='Текст')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='bot.BotUser', verbose_name='Гости жениха')),
            ],
            options={
                'verbose_name': 'Гости жениха',
                'verbose_name_plural': 'Гости жениха',
            },
        ),
        migrations.AlterUniqueTogether(
            name='guests',
            unique_together=set(),
        ),
        migrations.RemoveField(
            model_name='guests',
            name='user',
        ),
        migrations.DeleteModel(
            name='Guests',
        ),
    ]
