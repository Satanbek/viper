# Generated by Django 2.0.2 on 2018-03-05 21:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0033_auto_20180305_1545'),
    ]

    operations = [
        migrations.AddField(
            model_name='botconfiguration',
            name='start_message',
            field=models.TextField(blank=True, verbose_name='Ответ на "/start"'),
        ),
    ]
