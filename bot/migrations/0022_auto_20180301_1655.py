# Generated by Django 2.0.2 on 2018-03-01 13:55

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0021_auto_20180228_1242'),
    ]

    operations = [
        migrations.AlterField(
            model_name='guests',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot.BotUser', verbose_name='Гости'),
        ),
    ]
