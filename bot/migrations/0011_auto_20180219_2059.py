# Generated by Django 2.0.2 on 2018-02-19 17:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0010_auto_20180219_2055'),
    ]

    operations = [
        migrations.AlterField(
            model_name='specialist',
            name='photo_file_id',
            field=models.CharField(blank=True, max_length=128, null=True),
        ),
    ]
