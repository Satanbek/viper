# Generated by Django 2.0.2 on 2018-03-09 20:26

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0039_auto_20180309_2256'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='specialist',
            name='city',
        ),
        migrations.AddField(
            model_name='specialist',
            name='cities',
            field=models.ManyToManyField(related_name='specialists', to='bot.City', verbose_name='Города'),
        ),
    ]
