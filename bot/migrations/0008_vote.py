# Generated by Django 2.0.2 on 2018-02-19 16:14

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('bot', '0007_category_specialist'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.PositiveSmallIntegerField(choices=[('1', 'За'), ('0', 'Против')], default='1')),
                ('specialist', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='bot.Specialist')),
                ('user', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, to='bot.BotUser')),
            ],
        ),
    ]
