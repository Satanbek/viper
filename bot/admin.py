from django.contrib import admin
from django.contrib.auth.models import Group, User
from solo.admin import SingletonModelAdmin

from .models import (
    BotUser, Specialist, Category, Vote, Comment, Review, Promo,
    Charges, BrideGuests, GroomGuests, Feedback, SpecialistApplication,
    BotConfiguration, Exhibit, City)

admin.site.unregister(Group)
admin.site.unregister(User)

admin.site.site_header = 'Weddingbot Admin'


@admin.register(BotConfiguration)
class BotConfigurationAdmin(SingletonModelAdmin):
    autocomplete_fields = (
        'notify_about_feedback',
        'notify_about_specialist_application',
        'notify_about_out_of_views'
    )


class ChargesInline(admin.StackedInline):
    model = Charges
    fields = ('text',)


class BrideGuestsInline(admin.StackedInline):
    model = BrideGuests


class GroomGuestsInline(admin.StackedInline):
    model = GroomGuests


class ExhibitInline(admin.StackedInline):
    model = Exhibit
    extra = 0
    classes = ('collapse',)
    exclude = ('photo_file_id', 'video_file_id')


@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    search_fields = ('name',)


@admin.register(BotUser)
class BotUserAdmin(admin.ModelAdmin):
    list_display = (
        'chat_id', 'username',
        'name', 'city',
        'wedding_date'
    )

    exclude = ('pair_photo_file_id',)
    search_fields = ('chat_id', 'username', 'name')
    list_filter = ('wedding_date', 'city')
    autocomplete_fields = ('favorite_specialists', 'city')

    inlines = (
        ChargesInline,
        BrideGuestsInline,
        GroomGuestsInline
    )


@admin.register(Specialist)
class SpecialistAdmin(admin.ModelAdmin):
    exclude = ('photo_file_id', 'current_exhibit_id')
    list_display = ('name', 'allowed_views')
    search_fields = ('name', 'information', 'pricing', 'contacts')
    autocomplete_fields = ('categories', 'cities')
    list_filter = ('categories',)

    inlines = (
        ExhibitInline,
    )


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    search_fields = ('name',)


@admin.register(Vote)
class VoteAdmin(admin.ModelAdmin):
    list_display = ('user', 'status', 'specialist')
    search_fields = ('user', 'specialist')
    list_filter = ('status',)
    autocomplete_fields = ('user', 'specialist')


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = ('user', 'specialist')
    search_fields = ('user', 'specialist')
    autocomplete_fields = ('user', 'specialist')


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    exclude = ('photo_file_id',)


@admin.register(Promo)
class PromoAdmin(admin.ModelAdmin):
    exclude = ('photo_file_id',)


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    list_filter = ('is_processed', 'created_at')
    list_display = ('get_user_name', 'created_at', 'is_processed')
    readonly_fields = ('user', 'text')
    fields = ('user', 'text', 'is_processed')

    def get_user_name(self, obj):
        return obj.user.get_name

    get_user_name.short_description = 'Пользователь'


@admin.register(SpecialistApplication)
class FeedbackAdmin(admin.ModelAdmin):
    list_filter = ('is_processed', 'created_at')
    list_display = ('get_user_name', 'created_at', 'is_processed')
    readonly_fields = ('user', 'text')
    fields = ('user', 'text', 'is_processed')

    def get_user_name(self, obj):
        return obj.user.get_name

    get_user_name.short_description = 'Пользователь'
