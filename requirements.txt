certifi==2018.10.15
chardet==3.0.4

Django==2.1.2
django-solo==1.1.3
djangorestframework==3.6.2
idna==2.7
Pillow==5.3.0
pyTelegramBotAPI==3.6.6
pytz==2018.7
requests==2.20.0
six==1.11.0
urllib3==1.24
